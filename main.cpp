#include <iostream>

void mergesort(int arr1[], const int lenArr1, int arr2[], const int lenArr2);
void show(int arr[], const int lenArr);

int main()
{
    const int LenOne = 5;
    const int LenTwo = 4;
    int arr1[LenOne] = { 1, 4, 7, 9, 10 };
    int arr2[LenTwo] = { 2, 3, 5, 8 };

    mergesort(arr1, LenOne, arr2, LenTwo);

    show(arr1, LenOne);
    show(arr2, LenTwo);

    return 0;
}

void mergesort(int arr1[], const int lenArr1, int arr2[], const int lenArr2)
{
    for (int i = 0; i < lenArr1; i++) {
        if (arr1[i] > arr2[0]){
            int temp = arr1[i];
            arr1[i] = arr2[0];
            arr2[0] = temp;
        }

        // to sort second array after replacement arr2[0]
        int k; int first = arr2[0];
        for (k = 1; k < lenArr2 && arr2[k] < first; k++) {
            arr2[k - 1] = arr2[k];
        }

        arr2[k - 1] = first;
    }
}

void show(int arr[], const int lenArr)
{
    for (int i = 0; i < lenArr; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}
